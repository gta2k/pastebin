# PasteBin

This is an alternative to well-known [Pastebin](https://pastebin.com/)  
Allows sharing text data with someone by sending him a link. Text can be protected with password.

I made it to practice with Java / Spring Boot and Thymeleaf.

CI/CD is configured on both GitHub Actions and Bitbucket pipelines.

## Demo

I deployed the application to Google Cloud Platform. Check it [here](http://pastebin.demo.belous.dev/)

package com.example.pastebin.repository;

import com.example.pastebin.model.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface MessageRepository extends CrudRepository<Message, String> {

    List<Message> findAllByExpireAtBefore(LocalDateTime dateTime);

}

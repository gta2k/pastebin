package com.example.pastebin.exception;

public class InvalidMessageId extends RuntimeException {
    public InvalidMessageId() {
        super("This message is either expired or never existed");
    }
}

package com.example.pastebin.exception;

public class TooManyMessages extends RuntimeException {
    public TooManyMessages() {
        super("Too many messages created");
    }
}

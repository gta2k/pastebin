package com.example.pastebin.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class Message {
    @Id
    private String id;
    private String text;
    private String secret;
    private LocalDateTime expireAt;

    public Message(String text, String secret, LocalDateTime expireAt) {
        this.text = text;
        this.secret = secret;
        this.expireAt = expireAt;
    }

}

package com.example.pastebin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

@Configuration
public class DurationConfiguration {

    public SortedMap<Integer, String> durations = new TreeMap<>();

    @Value("${durations}")
    private List<Integer> durationConfig;

    @PostConstruct
    private void postProcessor() {
        for (int duration : durationConfig) {
            durations.putIfAbsent(duration, convertMinutes(duration));
        }
    }

    private String convertMinutes(int minutes) {
        String ending = minutes == 1 || (minutes / 60) == 1 ? "" : "s";
        return minutes < 60
                ? minutes + " minute" + ending
                : minutes / 60 + " hour" + ending;
    }

}

package com.example.pastebin.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public final class MessageDto {
    @NotBlank
    @Size(max = 100000)
    private String text;

    @Size(max = 20, message = "Too long. Make it less than 20 symbols")
    private String secret;

    @Min(1)
    @Max(24 * 60) //24 hours
    private Integer duration;

}

package com.example.pastebin.service;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SchedulerService {
    private final MessageService messageService;

    @Scheduled(fixedRate = 60 * 1000)
    public void RemoveExpired() {
        messageService.deleteExpired();
    }

}
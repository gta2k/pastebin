package com.example.pastebin.service;

import com.example.pastebin.exception.InvalidMessageId;
import com.example.pastebin.exception.TooManyMessages;
import com.example.pastebin.model.Message;
import com.example.pastebin.repository.MessageRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;

    @Value("${salt}")
    private String SALT;
    @Value("${id-length}")
    private Integer idLength;
    @Value("${max-messages}")
    private Integer MAX_NUMBER_OF_MESSAGES;

    public Message findById(String id) {
        return messageRepository.findById(id)
                .orElseThrow(InvalidMessageId::new);
    }

    public void deleteExpired() {
        messageRepository
                .findAllByExpireAtBefore(LocalDateTime.now())
                .forEach(messageRepository::delete);
    }

    public String add(String text, String secret, int expireIn) {
        validateNumberOfMessages();

        if (!secret.isEmpty()) {
            secret = sha256(secret);
        }
        LocalDateTime expireAt = LocalDateTime.now().plusMinutes(expireIn);
        Message message = new Message(text, secret, expireAt);
        message.setId(generateId());

        messageRepository.save(message);
        return message.getId();
    }

    public String sha256(String secret) {
        return DigestUtils.sha256Hex(secret + SALT);
    }

    private void validateNumberOfMessages() {
        if (messageRepository.count() >= MAX_NUMBER_OF_MESSAGES) {
            throw new TooManyMessages();
        }
    }

    private String generateId() {
        final char A = 'a';
        final char Z = 'z' + 1;

        return new Random().ints(A, Z)
                .limit(idLength)
                .collect(
                        StringBuilder::new,
                        StringBuilder::appendCodePoint,
                        StringBuilder::append)
                .toString();
    }

}

package com.example.pastebin.controller;

import com.example.pastebin.config.DurationConfiguration;
import com.example.pastebin.dto.MessageDto;
import com.example.pastebin.model.Message;
import com.example.pastebin.service.MessageService;
import io.micrometer.core.annotation.Counted;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@Timed("controller.response.time")
public class MessageController {

    private final DurationConfiguration durationConfig;
    private final MessageService messageService;

    @GetMapping("/")
    @Counted("number.of.visitors")
    public String addForm(Model model) {
        MessageDto dto = new MessageDto();
        model.addAttribute("messageDto", dto);
        model.addAttribute("durationList", durationConfig.durations);
        return "add-form";
    }

    @PostMapping("/")
    public String addFormProcess(@Valid MessageDto dto, Errors error, Model model) {
        if (error.hasErrors()) {
            model.addAttribute("durationList", durationConfig.durations);
            return "add-form";
        }

        String id = messageService.add(dto.getText(), dto.getSecret(), dto.getDuration());
        return "redirect:" + id;
    }

    @GetMapping("/{id}")
    public String show(@PathVariable("id") String id, Model model) {
        Message message = messageService.findById(id);

        if (message.getSecret().isEmpty()) {
            model.addAttribute("message", message.getText());
            return "show-message";
        }

        return "secret-required";
    }

    @PostMapping("/{id}")
    public String showSecret(@PathVariable("id") String id, @RequestParam String secret, Model model) {
        Message message = messageService.findById(id);

        if (message.getSecret().equals(messageService.sha256(secret))) {
            model.addAttribute("message", message.getText());
            return "show-message";
        }

        model.addAttribute("secret", secret);
        model.addAttribute("error", "Secret doesn't fit");
        return "secret-required";
    }

}

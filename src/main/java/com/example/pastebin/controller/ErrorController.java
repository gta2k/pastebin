package com.example.pastebin.controller;

import com.example.pastebin.exception.InvalidMessageId;
import com.example.pastebin.exception.TooManyMessages;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorController {

    @ExceptionHandler({
            InvalidMessageId.class,
            TooManyMessages.class,
    })
    public String errorHandler(Exception e, Model model) {
        model.addAttribute("error", e.getMessage());
        return "show-error";
    }

}

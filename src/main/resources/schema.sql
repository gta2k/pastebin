DROP TABLE IF EXISTS message;
CREATE TABLE message
(
    id        VARCHAR(36) PRIMARY KEY, -- UUID length
    text      VARCHAR     NOT NULL,
    secret    VARCHAR(64) NOT NULL, -- SHA256 length
    expire_at TIMESTAMP   NOT NULL
);
const x = setInterval(function () {
    const countDownDate = new Date(/*[[${transport.departureDate}]]*/).getTime();
    const index = [[${iterStat.index}]];

    console.log(index);

    const now = new Date().getTime();

    const distance = countDownDate - now;

    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    document.getElementById('timer-' + index).innerHTML = days + "d " +
        hours + "h " + minutes + "m " + seconds + "s ";

    if (distance < 0) {
        clearInterval(x);
        document.getElementById('timer-' + index).innerHTML = "EXPIRED";
    }
}, 1000);
#FROM bellsoft/liberica-openjdk-alpine
#ARG JAR_FILE=target/*.jar
#COPY ${JAR_FILE} app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]

FROM bellsoft/liberica-openjdk-alpine AS build
WORKDIR /build
COPY . /build
RUN sh mvnw clean package -DskipTests

FROM bellsoft/liberica-openjdk-alpine
EXPOSE 8080
COPY --from=build /build/target/*.jar app.jar
CMD ["java","-jar","app.jar"]
